package com.demo.multitenancy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Simple index controller.
 * 
 * @author Minhaj
 */
@Controller
@RequestMapping("/")
public class IndexController {

	@RequestMapping
	public String index() {
		return "index";
	}
}
