package com.demo.multitenancy.bean;

import org.springframework.data.repository.CrudRepository;

/**
 * @author Minhaj
 */

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

}
