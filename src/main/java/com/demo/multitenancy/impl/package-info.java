/**
 * Copyright (c) 2017 Minhaj.
 * 
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

/**
 * Multitenancy implementation details with custom classes.
 * 
 * @author Minhaj
 * @since 2017-08-28
 */
package com.demo.multitenancy.impl;
