package com.demo.multitenancy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author Minhaj
 */

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class MutitenantDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MutitenantDemoApplication.class, args);
	}
}
